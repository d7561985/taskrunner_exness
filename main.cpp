#include <iostream>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/bind.hpp>

#include "TaskLib/Task.h"
#include "TaskLib/TaskMgr.h"


/**
 *  For test result go to readme.md
 */

boost::atomic<bool> done(false);
boost::atomic_int producer_count(0);


static TaskMgr sTaskMgr(8);

void addTask();

void math() {
    int64_t r = 2;

    const bool v = (std::rand() % 2) == 0;
    for (int i = 0; i < 1000; ++i) {
        if (v)
            r += std::rand() % 2;
        else
            r *= std::rand() % 2;
    }

    //std::cout << "printA: "  << std::endl;

    ++producer_count;
    addTask();
}

//! Task exaple
void work() { math(); };

struct worker {
public:
    void operator()() { work(); };
};

class Work_class {
public:
    void build(int) {
        ++data;
        work();
    }

    int data = 0;
};

void more_work(int) { work(); };

Work_class *w = new Work_class();

void addTask() {
    switch (int(producer_count % 4)) {
        //! simple function
        case 0:
            sTaskMgr.addTask(new Task(work));
            break;
        //! static struct call(functor)
        case 1:
            sTaskMgr.addTask(new Task(worker()));
            break;
        //! send parametrs with std::bind
        case 2:
            sTaskMgr.addTask(new Task(std::bind(more_work, (int) producer_count)));
            break;
        //! dynamic link object call function with parameters.
        case 3:
            sTaskMgr.addTask(new Task(boost::bind(&Work_class::build, w, 100))); //<- gcc - support, msvc  - not.
            //! lambda call is suported by msvc compiler.
            //sTaskMgr.addTask(new Task([&](){w->build(producer_count);}));
            break;
        default:
            assert(false);
            break;
    }
}

int main() {
    //! TEST:: benchmark.
    boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();

    //! TEST:: add to queue something for start. it's test.
    for (int32_t i = 0; i < 10000; ++i)
        addTask();

    //! TEST:: count for testing.
    int last = producer_count;
    while (!done) {
        if (producer_count > 10000000)
            break;

        int diff = producer_count - last;
        last = producer_count;

        std::cout << "operations at 10 ms: " << diff << " cur: " << producer_count << " m count: " << w->data
                  << std::endl;
        boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }

    //TEST::
    boost::posix_time::time_duration diff = boost::posix_time::microsec_clock::local_time() - t1;
    std::cout << "Finished at: " << diff.total_milliseconds() << " ms. " << std::endl;

    return 0;
}