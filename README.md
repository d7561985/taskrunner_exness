# README #

### What is this repository for? ###

TaskMgr multithreaded task runner with use boost::lockfree::queue witch is thread safe and not use mutex.
some about boost::lockfree::queue - https://habrahabr.ru/post/209824/

### How do I get set up? ###

for configure test program use cmake. Require static library boost 1.64 with BOOST_ROOT link (if win)

### TEST DATA ###

 *  Simple mt test with adding at one queue new task while it executed.
 *  2 functuins printA & printB do it.
 *
 *  Test perfom  on win10 with 2 compilers: gcc 7.1.0 & msvc 14.1 with 2 options (logging with std::cout and withoud it).
 *  widh log printing (log has a lot of lock's and it's freezzz our task)
 *
  Some test results (on i7 6700K) release build:

** 1000000 + 10000 queue tasks (just highload of queue system)**
```
#!c++
 GCC with 8 cores: with log: 90269 ms (90.2 sec.), no log: 386ms (0.386 sec)
 GCC with 4 cores: with log: 88347 ms (88.3 sec.), no log: 194-209ms (0.209 sec)
 GCC with 2 cores: with log: 99197 ms (99.1 sec.), no log: 222-245ms (0.222 sec)

 MSVC14.1 32bit with 8 cores: with log: 120191 ms (120.0 sec.), no lot 485 ms (0.48 sec)
 MSVC14.1 32bit with 4 cores: with log: 124230 ms (124.2 sec.), no lot 343 ms (0.3 sec)
 MSVC14.1 32bit with 2 cores: with log: 140915 ms (140.0 sec.), no lot 367 ms (0.3 sec)
```

** 10000000 + 10000 queue task (just highload of queue system). sorry but with log it's take a lot of time so this test not perfomed:**


```
#!c++

 GCC with 8 cores: no log: 1888 ms (1.8 sec)
 GCC with 4 cores: no log: 1826 ms (1.8 sec)
 GCC with 2 cores: no log: 2153 ms (2.153 sec)

 MSVC14.1 32bit with 8 cores: no log 4862 ms (4.8 sec)
 MSVC14.1 32bit with 4 cores: no log 3237 ms (3.2 sec)
 MSVC14.1 32bit with 2 cores: no log 3471 ms (3.4 sec)
```



** 10000000 + some mathematical at task functions. this real mt profit ;)**
```
#!c++
 GCC with 8 cores: no log: 17175 ms (17.1 sec)
 GCC with 4 cores: no log: 26159 ms (26.1 sec)
 GCC with 2 cores: no log: 43786 ms (43.7 sec)

 MSVC14.1 32bit with 8 cores: no log 40530 ms (40.5 sec)
 MSVC14.1 32bit with 4 cores: no log 54542 ms (54.5 sec)
 MSVC14.1 32bit with 2 cores: no log 84567 ms (84.5 sec)

 MSVC14.1 64bit with 8 cores: no log 28610 ms (28.6 sec)
 MSVC14.1 64bit with 4 cores: no log 45971 ms (45.9 sec)
 MSVC14.1 64bit with 2 cores: no log 75722 ms (75.7 sec)
```