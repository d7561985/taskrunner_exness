//
// Created by d7561985@gmail.com on 01.07.2017.
//

#ifndef TASKRUNNER_TASK_H
#define TASKRUNNER_TASK_H

#pragma once

#include <memory>
#include <functional>

/**
 * Task object for TaskMgr.
 * Require: Create object in heap memory(new operator).
 * Delete on heap created object - part of TaskMgr.
 */
class Task {
public:
    /**
     *
     * Constructor take as param. any std::function.
     * @param task - any function
     */
    template<typename TTask>
    Task(TTask task): task(std::make_shared<std::function<void()>>(task)) {}

    void run() {
        task->operator()();
    }

protected:
    std::shared_ptr<std::function<void()>> task;
};

#endif //TASKRUNNER_TASK_H