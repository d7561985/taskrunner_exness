//
// Created by d7561985@gmail.com on 01.07.2017.
//

#include "TaskMgr.h"
#include "Task.h"

TaskMgr::TaskMgr(int n_cores/* = 1*/) : queue(65534), running(true) {
    //! check construct parameter of numbers of cores.
    if (n_cores < 1)
        n_cores = 1;

    //! Run threads!!! Reactors now are working.
    for (int i = 0; i < n_cores; ++i)
        threads.create_thread(boost::bind(&TaskMgr::Worker, this));
}

TaskMgr::~TaskMgr() {
    Stop();

    //! free queue if something exist.
    Task *value;
    while (this->queue.pop(value))
        delete value;
}

bool TaskMgr::addTask(Task *t) {

    //! if progremm at shutting down state - not add ne Task's.
    if (!running) {
        delete t;
        return false;
    }

    while (!queue.push(t));
    return true;
}

void TaskMgr::Stop() {
    if (!running)
        return;

    running = false;
    threads.join_all();
}

void TaskMgr::Worker() {
    Task *value;
    while (this->running) {
        //! as we use lockfree method no need to force sleep thread, but we should do this for reduct cpu usage.
        //!  for better perfomance we can use specialized reactors.
        boost::this_thread::sleep(boost::posix_time::milliseconds(10));

        while (this->queue.pop(value)) {
            value->run();
            delete value;
        }
    }
}