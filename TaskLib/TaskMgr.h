//
// Created by d7561985@gmail.com on 01.07.2017.
//

#pragma once
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/atomic.hpp>

#ifndef TASKRUNNER_TASKMGR_H
#define TASKRUNNER_TASKMGR_H

class Task;

/**
 *  TaskMgr multithreaded task runner with use boost::lockfree::queue witch is thread safe and not use mutex.
 *  some about boost::lockfree::queue - https://habrahabr.ru/post/209824/
 * For using lockfree we need encapsulate our task on special object perfomed on Task class.
 */
class TaskMgr
{
public:
    /**
     * At constructor we create new thread task witch call reactor TaskMgr::Worker
     * @param n_cores
     */
    TaskMgr(int n_cores = 1);

    /**
     * all thread's stop correctly.
     */
    ~TaskMgr();
    /**
     *
     * @param t - Task object.
     */
    bool addTask(Task* t);

protected:
    void Worker();
    void Stop();

    boost::thread_group threads;
    boost::lockfree::queue<Task *, boost::lockfree::fixed_sized<true>> queue;
    boost::atomic<bool> running;
};

#endif //TASKRUNNER_TASKMGR_H
